	$('.carousel').carousel({
		interval: 2500
	});
	
	function addOptions() {
        var jsonArray = suggestions;
		var select = document.getElementById('suggestions');
        var option;
        while (select.firstChild) {
		    select.removeChild(select.firstChild);
		}

       	for (var i = 0; i < jsonArray.length; i++) {
            option = document.createElement('a');
            option.setAttribute('href','#');
            option.className = "category";
            option.textContent = jsonArray[i]["Mode"];
            select.appendChild(option);
        }
        select.classList.toggle("show");
    };  

	window.addEventListener('click', function(e){   
		e = e || window.event;
	    var target = e.target || e.srcElement;

		var select = document.getElementById('suggestions');
		var menuBurger = document.getElementsByClassName('burger-menu-dropdown')[0];

		if (!document.getElementById('burger-menu').contains(e.target)){
			menuBurger.style.display = 'none';
		}

		if (!document.getElementsByClassName('search')[0].contains(e.target)){
			select.classList.remove('show');
		} 
	});

	window.addEventListener('click', function(e) {
	    e = e || window.event;
	    var target = e.target || e.srcElement;
	    
	    if (target.classList[0] == 'widget-flickr-link') {
	    	var m_ID = target.classList;
            document.getElementById(m_ID[1]).style.display = 'block';
	    }

	    if (target.classList.contains('flickr-modal')) {
	        target.style.display = 'none';
	    }
	}, false);


	var reg = document.getElementById('modal-registration');
	var btn = document.getElementById("xlogin");
	var span = document.getElementsByClassName("close")[0];
	var triggerLog = document.getElementById("login-trigger");
	var triggerReg = document.getElementById("registration-trigger");
	var log = document.getElementById('modal-login');
	var menuBurger = document.getElementById('burger-menu');
	var menuMobile = document.getElementsByClassName('burger-menu-dropdown')[0];

	menuBurger.onclick = function() {
		menuMobile.style.display = 'block'
	}

	btn.onclick = function() {
	    reg.style.display = "block";
	};

	window.onclick = function(event) {
	    if (event.target == reg || event.target == log) {
	        reg.style.display = "none";
	        log.style.display = "none";
	    }
	};

	triggerLog.onclick = function() {
	    reg.style.display = "none";
	    log.style.display = "block";
	};

	triggerReg.onclick = function() {
	    reg.style.display = "block";
	    log.style.display = "none";
	};

	// Blog
	
	function hide(classOfElements) {
		var item = document.getElementsByClassName(classOfElements);

		for (var i = 1; i < item.length; i++) {
			var currentItem = item[i].classList[1];
			if (currentItem == 'hide') {
				setTimeout(function(x) { 
					return function() {
						item[x-2].classList.remove("show");
						item[x-2].classList.toggle("hide");
						item[x].classList.remove("hide");
						item[x].classList.toggle("show");
					};
				}(i), 5000*i);
			}
		}
	};

	hide("main-addon-news");
	hide("twitter-section");

		/* Save emails to Local Storage */
	// window.localStorage.removeItem('emailsList');

	function saveEmailToLocalStorage (){
		var emailsList = 'emailsList';
		var data = JSON.parse(localStorage.getItem(emailsList)) || [];
		var btnSaveEmail = document.getElementById('btnSaveEmail');
	    var inputVal = document.getElementById('inputEmail').value;
	    var isValidEmail = /^[^@]+@[^@.]+\.[^@]*\w\w$/.test(inputVal);
		var email = {};

	    if(isValidEmail){
		    email.address = inputVal
		    data.push(email);
			localStorage.setItem(emailsList, JSON.stringify(data));
			document.getElementById('inputEmail').value = '';
	    }else{
	    	alert('Please enter valid email address.');
	    }
	    console.log(data);
	}